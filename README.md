# README #

This repo demonstrates two main tasks:

1. fetching PositionDetails from PreCise API.

2. translating those PositionDetails to PointGraphics on an ESRI Map.


### Trickery ###

* I've setup our API to accept CORS requests from http://127.0.0.1:5500 so make sure you run the index.html from that domain & port.

* The easiest way I've found is through Visual Studio Code's Live Server.

### What is next? ###

* (DONE) move all functions (like the fetch) that don't use esri to a scripts/ directory.
* (DONE) add loading bar when waiting on fetch, use promises to control the div :)
* automated the map zoom based on the points we get back from fetch.
* point models!!!
* ui dropdown to allow users to change basemap based on selection.
* ui dropdown to allow users to change the point colors based on a selection.
* more esri stuff to make it look pretty!!!


### Repo Owner ###

* joseph connelly, precisemrm 2022