
/**
 * Utility for converting hex colors into rgb objects.
 */
function HexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

/**
 * Fetch last know asset position data from precise api running locally.
 */
async function FetchPositions() {

    const url = 'http://localhost:57194/api/v202007/companies/182/Reports/RawDataByReceivedTime';

    const postBody = {
        StartDateTime: '2021-12-06T07:00:00.000Z',
        EndDateTime: '2021-12-06T08:20:00.000Z',
        AssetIds: [66883]
    }

    const corsRequestHeader = {
        'Accept': '*/*',
        'Content-Type': 'application/json; charset=utf-8',
        'Origin': 'http://127.0.0.1:5500',
        'User-Name': 'jconnelly@precisemrm.com',
        'Api-Key': 'dfda23878abdc083a0714ccad6811f0216b4ff8b213200d7331319a759d80df9'
    };

    const options = {
        method: 'POST',
        mode: 'cors',
        cache: 'force-cache',
        credentials: 'include',
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(postBody), 
        headers: corsRequestHeader
    };

    // Force this function to wait until the server responses to our request.
    const response = await fetch(url, options);

    if (!response.ok) {
        throw new Error("Network Error: HTTP Status=" + response.status);
    }

    const positionDetails = await response.json();

    return positionDetails;
} 